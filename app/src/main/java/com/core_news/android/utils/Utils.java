package com.core_news.android.utils;

import android.util.Log;

import com.core_news.android.BuildConfig;


/**
 * Created by andrii on 03/03/2016.
 */
public class Utils {
    public static void debugLog(String TAG, String message) {
        if (BuildConfig.DEBUG) Log.d(TAG, message);
    }
}
